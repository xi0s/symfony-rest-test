FROM composer as builder
WORKDIR /app/

ENV APP_ENV prod

COPY bin/ ./bin/
COPY public/ ./public/
COPY templates/ ./templates/
COPY .env ./
COPY composer.* ./
COPY symfony.lock ./
COPY config/ ./config/
COPY tests/ ./tests/
COPY src/ ./src/

RUN composer install --no-dev --ignore-platform-reqs

FROM php:7-apache

WORKDIR /app

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod uga+x /usr/local/bin/install-php-extensions && sync && \
  install-php-extensions \
  apcu \
  mailparse \
  mysqli \
  pdo_mysql \
  gd \
  uuid

ENV APACHE_DOCUMENT_ROOT /app/public

RUN a2enmod rewrite

LABEL version="0.1.0" \
      maintainer="callumsmith@me.com"

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf && \
  sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

ENV APP_ENV prod

COPY --from=builder /app/ /app/
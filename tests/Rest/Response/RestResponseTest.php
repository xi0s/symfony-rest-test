<?php

/**
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2021 Callum Smith-
 */

namespace App\Tests\Rest\Response;

use App\Rest\Response\RestResponse;
use PHPUnit\Framework\TestCase;

class RestResponseTest extends TestCase
{

  protected $testBody = [
    'response_code' => 404,
    'error' => "Page not found",
  ];

  protected $testResponseCode = 404;

  function testGetters()
  {
    $response = new RestResponse($this->testBody, $this->testResponseCode);
    $this->assertEquals($this->testBody, $response->getBody());
    $this->assertEquals($this->testResponseCode, $response->getResponseCode());
  }

  function testSetters()
  {
    $response = new RestResponse();
    $this->assertEquals(null, $response->getBody());
    $this->assertEquals(200, $response->getResponseCode());
    $this->assertNotTrue($response->hasBody());
    $response->setBody($this->testBody);
    $response->setResponseCode($this->testResponseCode);
    $this->assertEquals($this->testBody, $response->getBody());
    $this->assertEquals($this->testResponseCode, $response->getResponseCode());
    $this->assertTrue($response->hasBody());
  }
}

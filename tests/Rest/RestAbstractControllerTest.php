<?php

/**
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2021 Callum Smith-
 */

namespace App\Tests\Rest;

use App\Rest\Exception\RestAuthenticationException;
use App\Rest\Exception\RestException;
use App\Rest\Response\RestResponse;
use App\Rest\RestAbstractController;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RestAbstractControllerTest extends TestCase
{

  private function createInstance(): RestAbstractController
  {
    return new class extends RestAbstractController {
      public function __invoke()
      {
        return new RestResponse(['message' => 'PHP-Unit success']);
      }
    };
  }

  private function createErroringInstance(): RestAbstractController
  {
    return new class extends RestAbstractController {
      public function __invoke()
      {
        throw new RestException('An error message', Response::HTTP_BAD_REQUEST);
      }
    };
  }

  private function createAuthNErrorInstance(): RestAbstractController
  {
    return new class extends RestAbstractController {
      public function __invoke()
      {
        throw new RestAuthenticationException('An error message');
      }
    };
  }
    
  public function testAbstractClassMethod()
  {
      // Let's test the public function we created in the anonymous class
      $this->assertInstanceOf(
          RestAbstractController::class, 
          $this->createInstance()
      );
  }
  
  public function testParseRequest()
  {
    $testContent = [
      'test_id' => 1212,
      'string_content' => "What is the meaning of string",
    ];
    // Test GET method JSON parsing
    $restInstance = $this->createInstance();
    $request = new Request([], [], [], [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($testContent));
    $updatedRequest = $restInstance->parseRequest($request);
    $this->assertEquals($testContent, $updatedRequest->query->all());
    // Test POST method JSON parsing
    $restInstance = $this->createInstance();
    $request = Request::create('', 'POST', [], [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($testContent));
    $updatedRequest = $restInstance->parseRequest($request);
    $this->assertEquals($testContent, $updatedRequest->request->all());
  }
  
  public function testHeaders()
  {
    $headers = [
      'X-PHPUnit-test' => "Passed",
      'X-API-Version' => "0.1.0",
    ];
    $instance = $this->createInstance();
    foreach($headers as $header => $content){
      $instance->addHeader($header, $content);
    }
    $response = $instance->getResponse();
    foreach($headers as $header => $content){
      $this->assertArrayHasKey(strtolower($header), $response->headers->all());
      $this->assertEquals($content, $response->headers->get($header));
    }
  }

  public function testResponse()
  {
    $instance = $this->createInstance();
    $response = $instance->getResponse();
    $mockError = new RestResponse(['message' => 'PHP-Unit success']);
    $this->assertEquals($mockError->getResponseCode(), $response->getStatusCode());
    $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    $this->assertEquals(json_encode($mockError, ($_SERVER['APP_DEBUG'] ? JSON_PRETTY_PRINT : 0)), $response->getContent());
  }

  // Errors generated will include a complex stack-trace that would need to be simulated and captured which is very hard to do, so a body comparison is not included here
  public function testErrorResponse()
  {
    $instance = $this->createErroringInstance();
    $response = $instance->getResponse();
    $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
  }

  // Errors generated will include a complex stack-trace that would need to be simulated and captured which is very hard to do, so a body comparison is not included here
  public function testAuthenticationResponse()
  {
    $instance = $this->createAuthNErrorInstance();
    $response = $instance->getResponse();
    $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
  }
}

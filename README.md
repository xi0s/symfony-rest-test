# Symfony REST Test

A pilot project using the Symfony full framework rather than just components. This app create an Expense model to keep track of expenses. In a more true-to-life application you would probably want to capute more data about the Expense including dates, locations and attaching images of receipts. However the role of this demo is to illustrate the capabilities of Symfony as a framework to instantiate small, testable, APIs in rapid fashion.

No authentication or authorization layer is built into this API but scaffolding for handling errors in a graceful way is in place. API documentation includes examples of how these errors would be sent to the client however are not fully implemented, so these responses cannot be accessed from the API frontend, even if you pass a dodgy bearer token.

## Running the service in a container

A docker-compose file is provided for convenience. If you are expecting to run this container in production it is recommended to run the container manually and provide an external database to connect to.

### First run of docker-compose locally

You will need to provision your database tables using the doctrine CLI tool. Use the following two commands to run the service and to instantiate your database tables.

```
docker-compose up
docker exec symfony-rest-test_expenses_1 /app/bin/console doctrine:schema:update --force
```

### Environment Variables

Variable | Description | Example Format
--- | --- | ---
DATABASE_URL | PDO-compatible database connection string | mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7
APP_ENV | Symfony environment | prod
APP_DEBUG | Global debug control | false

## Accessing the API documentation

The APIs exposed through this tool are fully documented using OpenAPI. Go to the following URLs to access the API documentation either in HTML or JSON format.

http://localhost:8888/api/v1/doc

http://localhost:8888/api/v1/doc.json

## Continuous integration

A number of Gitlab-CI steps have been setup with multiple-stages.

Stage 1.

PHP-Unit is run against the code. Coverage has not been included for the purposes of this demo.

Stage 2.

Integration tests are performed using a Postman suite of tests. These are designed to run from a blank database. A temporary build container is created.

Stage 3.

A final container is built and published to the gitlab repository.

Potential optimisations exist throughout the CI process, however as with all aspects of this demo this has been put together to illustrate a multi-stage test and build process. Ideally build artefacts would be carried forward from previous stages such as composer compiles and containers to reduce the pipeline runtime and cost. Additionally caching of intermediary containers would be beneficial.

Stage 1 in particular also benefit hugely from having pre-compiled php-unit capable image to run the unit tests against the software. The phpunit/phpunit containers are woefully out of date, so maintaining a local library of up-to-date containers would be a worthwhile step to reduce compile overheads in testing
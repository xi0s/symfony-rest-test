<?php

/**
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2021 Callum Smith-
 */

namespace App\Rest\Response;

use JsonSerializable;

class RestResponse implements JsonSerializable
{
  protected $body = [];
  protected $response_code = 200;

  /**
   * Undocumented function
   *
   * @param array|null An array formatted body to go for a response or null
   * @param integer HTTP response code
   */
  public function __construct(?array $body = null, int $responseCode = 200)
  {
    $this->setBody($body);
    $this->setResponseCode($responseCode);
  }

  /**
   * Get the response body
   *
   * @return array|null
   */
  function getBody(): ?array
  {
    return $this->body;
  }

  /**
   * Set the body of the response
   *
   * @param array|null $content
   * @return void
   */
  function setBody(?array $content): void
  {
    $this->body = $content;
  }

  /**
   * Set the body to a structured array format based on Google specificaiton
   * https://google.github.io/styleguide/jsoncstyleguide.xml
   *
   * @param array $data
   * @param integer $start
   * @param integer $length
   * @param integer $count
   * @return void
   */
  function setStructuredBody(array $data, ?int $start = 0, ?int $length = 0, ?int $count = 0): void
  {
    $output = ['data' => []];

    $output['data']['startIndex']     = $start ?? 0;
    $output['data']['totalItems']     = $count ?? count($data);
    $output['data']['itemsPerPage']   = $length ?? $output['data']['totalItems'];
    $output['data']['pageIndex']      = $length ? floor((int) $start / (int) $length) : 0;
    $output['data']['totalPages']     = $length ? ceil(($count ? $count : count($data)) / $length) : 1;
    $output['data']['items']          = $data;

    $this->body = $output;
  }


  /**
   * Test whether the response contains a body or not
   *
   * @return boolean
   */
  function hasBody(): bool
  {
    return !is_null($this->body);
  }

  /**
   * Get the HTTP response code
   *
   * @return integer
   */
  function getResponseCode(): int
  {
    return $this->response_code;
  }

  /**
   * Set the HTTP response code
   *
   * @param integer $responseCode
   * @return void
   */
  function setResponseCode(int $responseCode): void
  {
    $this->response_code = $responseCode;
  }

  /**
   * Get an array to be handled by a json_encode call
   *
   * @return array
   */
  function jsonSerialize(): array
  {
    $response = $this->body;
    $response['response_code'] = $this->response_code;
    return $response;
  }
}

<?php

/**
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2021 Callum Smith-
 */

namespace App\Rest;

use App\Rest\Exception\RestAuthenticationException;
use App\Rest\Response\RestResponse;
use JsonSerializable;
use Exception;
use Error;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class RestAbstractController extends AbstractController implements JsonSerializable
{
  // @var array
  protected $headers = [];
  // @var Request
  protected $request;

  /**
   * Undocumented function
   *
   * @param Request|null $request
   */
  public function __construct(Request $request = null)
  {
    // Load the default request object
    $this->loadRequest($request ?? Request::createFromGlobals());

    // Load some default headers
    // @todo Add CORS projection headers here
    $this->addHeader('Content-Type', 'application/json');
  }

  /**
   * Load and parse a Request object
   *
   * @param Request $request
   * @return void
   */
  protected function loadRequest(Request $request): void
  {
    $this->request = $this->parseRequest($request);
  }

  /**
   * Parses a Request object to handle raw JSON inputs
   *
   * @param Request $request
   * @return Request
   */
  public function parseRequest(Request $request): Request
  {
    if (0 === strpos($request->headers->get('CONTENT_TYPE'), 'application/json')) {
      $data = json_decode($request->getContent(), true);
      if ($request->getMethod() === 'GET') {
        $request = $request->duplicate($data);
      } elseif (in_array($request->getMethod(), ['POST', 'PUT', 'PATCH', 'DELETE'])) {
        $request = $request->duplicate([], $data);
      }
    }
    return $request;
  }

  /**
   * Add HTTP header to the response, if one already exists it's content will be overwritten without warning
   *
   * @param string $header
   * @param string $value
   * @return void
   */
  public function addHeader(string $header, string $value): void
  {
    $this->headers[$header] = $value;
  }

  /**
   * Generate RestResponse from authentication Exception or Error
   *
   * @param RestAuthenticationException $e
   * @return RestResponse
   */
  protected function generateAuthentcationErrorResponse(RestAuthenticationException $e): RestResponse
  {
    $this->addHeader('WWW-Authenticate', 'Bearer realm="API Access"');
    return $this->generateErrorResponse($e);
  }

  /**
   * Generate a formatted error array from an Exception or Error
   *
   * @param Exception|Error $e
   * @return Array
   */
  protected function generateErrorResponse($e): RestResponse
  {
    $output = ['error' => $e->getMessage()];
    if ($_SERVER['APP_DEBUG']) {
      $output['stack_trace'] = $e->getTrace();
    }
    return new RestResponse($output, (int) $e->getCode() ?? Response::HTTP_INTERNAL_SERVER_ERROR);
  }

  /**
   * Render the REST component calling a defined functionand returning a Response with error handling
   *
   * @param string Function name to call when generating output
   * @return Response
   */
  public function getResponse(string $function = '__invoke', ...$functionArguments): Response
  {
    try {
      $output = $this->$function(...$functionArguments);
      if(!($output instanceof RestResponse)){
        throw new Exception("The function '{$function}' did not return a RestResponse", Response::HTTP_INTERNAL_SERVER_ERROR);
      }
    } catch (RestAuthenticationException $e) {
      $output = $this->generateAuthentcationErrorResponse($e);
    } catch (Exception $e) {
      $output = $this->generateErrorResponse($e);
    } catch (Error $e) {
      $output = $this->generateErrorResponse($e);
    }
    $responseText = $output->hasBody() ? $output : null;
    $responseJson = ($_SERVER['APP_DEBUG'] ? json_encode($output, JSON_PRETTY_PRINT) : json_encode($output));
    return new Response($responseJson, ($output->getResponseCode() ? $output->getResponseCode() : 500), $this->headers);
  }

  

  /**
   * Return an OpenAPI reference for the REST endpoint
   */
  public function jsonSerialize()
  {
    return ['test'];
  }
}

<?php

/**
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2021 Callum Smith-
 */

namespace App\Rest\Exception;

use Symfony\Component\HttpFoundation\Response;

class RestAuthenticationException extends RestException
{
  public function __construct(string $message)
  {
    parent::__construct($message, Response::HTTP_UNAUTHORIZED);
  }
}

<?php

/**
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2021 Callum Smith-
 */

namespace App\Rest\Exception;

use RuntimeException;

class RestException extends RuntimeException
{
}

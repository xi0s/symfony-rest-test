<?php

/**
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2021 Callum Smith-
 */

namespace App\Controller;

use App\Entity\Expense;
use App\Repository\ExpenseRepository;
use App\Rest\Exception\RestException;
use App\Rest\Response\RestResponse;
use App\Rest\RestAbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/api/v1/expense")
 */
class ExpenseController extends RestAbstractController
{
  /**
   * List all of the expenses
   * 
   * Filters are not included on this endpoint but could be added trivially as an additonal parameter set.
   * 
   * @Route("/", name="expense_list", methods={"GET"})
   * @OA\Response(
   *    response=200,
   *    description="Returns a list of expenses",
   *    @OA\JsonContent(
   *      @OA\Property(
   *        property="data",
   *        type="array",
   *        @OA\Items(ref=@Model(type=Expense::class))
   *      ),
   *      @OA\Property(
   *        property="response_code",
   *        type="integer",
   *        description="Contains an integer reference to the HTTP response code",
   *        example=200
   *      )
   *    )
   * )
   * @OA\Response(response=500,description="API Exception",@OA\JsonContent(ref="#/components/schemas/APIErrorResponse"))
   * @OA\Response(response=401,description="Authentication Exception",@OA\JsonContent(ref="#/components/schemas/AuthNErrorResponse"))
   * @OA\Response(response=403,description="Authorization Exception",@OA\JsonContent(ref="#/components/schemas/AuthZErrorResponse"))
   * @OA\Parameter(
   *    name="length",
   *    in="query",
   *    description="Number of records to display",
   *    @OA\Schema(type="integer")
   * )
   * @OA\Parameter(
   *    name="start",
   *    in="query",
   *    description="Zero-index offset to start the page from",
   *    @OA\Schema(type="integer")
   * )
   */
  public function list(ExpenseRepository $expenseRepository): Response
  {
    return $this->getResponse('listResponse', $expenseRepository);
  }

  protected function listResponse(ExpenseRepository $expenseRepository): RestResponse
  {
    $start  = $this->request->query->get('start', null);
    $length = $this->request->query->get('length', null);
    $data   = $expenseRepository->findBy([], null, $length, $start);
    $total  = $expenseRepository->count([]);
    $response = new RestResponse();
    $response->setStructuredBody($data, $start, $length, $total);
    return $response;
  }

  /**
   * Create an expense
   * 
   * @Route("/", name="expense_create", methods={"POST"})
   * @OA\Response(
   *    response=201,
   *    description="Expense created",
   *    @OA\JsonContent(
   *      @OA\Property(
   *        property="data",
   *        ref=@Model(type=Expense::class)
   *      ),
   *      @OA\Property(
   *        property="response_code",
   *        type="integer",
   *        description="Contains an integer reference to the HTTP response code",
   *        example=201
   *      )
   *    )
   * )
   * @OA\Response(response=500,description="API Exception",@OA\JsonContent(ref="#/components/schemas/APIErrorResponse"))
   * @OA\Response(response=401,description="Authentication Exception",@OA\JsonContent(ref="#/components/schemas/AuthNErrorResponse"))
   * @OA\Response(response=403,description="Authorization Exception",@OA\JsonContent(ref="#/components/schemas/AuthZErrorResponse"))
   * @OA\Parameter(
   *    name="name",
   *    in="query",
   *    required=true,
   *    @OA\Schema(ref="#/components/schemas/Expense/properties/name")
   * )
   * @OA\Parameter(
   *    name="value",
   *    in="query",
   *    required=true,
   *    @OA\Schema(ref="#/components/schemas/Expense/properties/value")
   * )
   * @OA\Parameter(
   *    name="description",
   *    in="query",
   *    @OA\Schema(ref="#/components/schemas/Expense/properties/description")
   * )
   */
  public function create(Request $request, ValidatorInterface $validator): Response
  {
    return $this->getResponse('createResponse', $request, $validator);
  }

  protected function createResponse(Request $request, ValidatorInterface $validator, ?Expense $expense = null, int $responseCode = Response::HTTP_CREATED): RestResponse
  {
    $this->request = $this->parseRequest($request);
    
    if(!$expense){
      $expense = new Expense();
    }
    $expense->setName($this->request->request->get('name'));
    $expense->setValue($this->request->request->get('value'));
    $expense->setDescription($this->request->request->get('description'));

    if(!$validator->validate($expense)){
      throw new RestException('Values provided for the expense were not valid', Response::HTTP_BAD_REQUEST);
    }

    $entityManager = $this->getDoctrine()->getManager();
    $entityManager->persist($expense);
    $entityManager->flush();

    $expenseUrl = $this->generateUrl('expense_read', ['id' => $expense->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
    $this->addHeader('Location', $expenseUrl);

    $response = new RestResponse(['data' => $expense->jsonSerialize()], $responseCode);
    return $response;
  }

  /**
   * Get an individual expense
   * 
   * @Route("/{id}", name="expense_read", methods={"GET"}, requirements={"id"="\d+"})
   * @OA\Response(
   *    response=200,
   *    description="Individual expense record",
   *    @OA\JsonContent(
   *      @OA\Property(
   *        property="data",
   *        ref=@Model(type=Expense::class)
   *      ),
   *      @OA\Property(
   *        property="response_code",
   *        type="integer",
   *        description="Contains an integer reference to the HTTP response code",
   *        example=201
   *      )
   *    )
   * )
   * @OA\Response(response=500,description="API Exception",@OA\JsonContent(ref="#/components/schemas/APIErrorResponse"))
   * @OA\Response(response=401,description="Authentication Exception",@OA\JsonContent(ref="#/components/schemas/AuthNErrorResponse"))
   * @OA\Response(response=403,description="Authorization Exception",@OA\JsonContent(ref="#/components/schemas/AuthZErrorResponse"))
   */
  public function read(Expense $expense): Response
  {
    return $this->getResponse('readResponse', $expense);
  }

  protected function readResponse(Expense $expense): RestResponse
  {
    $response = new RestResponse(['data' => $expense->jsonSerialize()]);
    return $response;
  }

  /**
   * Update an expense
   * 
   * Any existing parameters will be over-ridden with given data, any missing parameters will be considered to be null.
   * An object contianing the newly updated expense will be returned.
   * 
   * @Route("/{id}", name="expense_update", methods={"PUT"}, requirements={"id"="\d+"})
   * @OA\Response(
   *    response=200,
   *    description="Expense updated",
   *    @OA\JsonContent(
   *      @OA\Property(
   *        property="data",
   *        ref=@Model(type=Expense::class)
   *      ),
   *      @OA\Property(
   *        property="response_code",
   *        type="integer",
   *        description="Contains an integer reference to the HTTP response code",
   *        example=200
   *      )
   *    )
   * )
   * @OA\Response(response=500,description="API Exception",@OA\JsonContent(ref="#/components/schemas/APIErrorResponse"))
   * @OA\Response(response=401,description="Authentication Exception",@OA\JsonContent(ref="#/components/schemas/AuthNErrorResponse"))
   * @OA\Response(response=403,description="Authorization Exception",@OA\JsonContent(ref="#/components/schemas/AuthZErrorResponse"))
   * @OA\Parameter(
   *    name="name",
   *    in="query",
   *    required=true,
   *    @OA\Schema(ref="#/components/schemas/Expense/properties/name")
   * )
   * @OA\Parameter(
   *    name="value",
   *    in="query",
   *    required=true,
   *    @OA\Schema(ref="#/components/schemas/Expense/properties/value")
   * )
   * @OA\Parameter(
   *    name="description",
   *    in="query",
   *    @OA\Schema(ref="#/components/schemas/Expense/properties/description")
   * )
   */
  public function update(Expense $expense, Request $request, ValidatorInterface $validator): Response
  {
    return $this->getResponse('createResponse', $request, $validator, $expense, Response::HTTP_OK);
  }

  /**
   * Update an expense
   * 
   * Any missing parameters will not be processed, only parameters passed will be updated against the record.
   * An object contianing the newly updated expense will be returned.
   * 
   * @Route("/{id}", name="expense_update", methods={"PATCH"}, requirements={"id"="\d+"})
   * @OA\Response(
   *    response=200,
   *    description="Expense created",
   *    @OA\JsonContent(
   *      @OA\Property(
   *        property="data",
   *        ref=@Model(type=Expense::class)
   *      ),
   *      @OA\Property(
   *        property="response_code",
   *        type="integer",
   *        description="Contains an integer reference to the HTTP response code",
   *        example=200
   *      )
   *    )
   * )
   * @OA\Response(response=500,description="API Exception",@OA\JsonContent(ref="#/components/schemas/APIErrorResponse"))
   * @OA\Response(response=401,description="Authentication Exception",@OA\JsonContent(ref="#/components/schemas/AuthNErrorResponse"))
   * @OA\Response(response=403,description="Authorization Exception",@OA\JsonContent(ref="#/components/schemas/AuthZErrorResponse"))
   * @OA\Parameter(
   *    name="name",
   *    in="query",
   *    @OA\Schema(ref="#/components/schemas/Expense/properties/name")
   * )
   * @OA\Parameter(
   *    name="value",
   *    in="query",
   *    @OA\Schema(ref="#/components/schemas/Expense/properties/value")
   * )
   * @OA\Parameter(
   *    name="description",
   *    in="query",
   *    @OA\Schema(ref="#/components/schemas/Expense/properties/description")
   * )
   */
  public function patch(Expense $expense, Request $request, ValidatorInterface $validator): Response
  {
    return $this->getResponse('patchResponse', $expense, $request, $validator);
  }

  protected function patchResponse(Expense $expense, Request $request, ValidatorInterface $validator): RestResponse
  {
    $this->request = $this->parseRequest($request);
    
    $expense = new Expense();
    if($this->request->request->has('name')){
      $expense->setName($this->request->request->get('name'));
    }
    if($this->request->request->has('value')){
      $expense->setValue($this->request->request->get('value'));
    }
    if($this->request->request->has('description')){
      $expense->setDescription($this->request->request->get('description'));
    }

    if(!$validator->validate($expense)){
      throw new RestException('Values provided for the expense were not valid', Response::HTTP_BAD_REQUEST);
    }

    $entityManager = $this->getDoctrine()->getManager();
    $entityManager->persist($expense);
    $entityManager->flush();

    $response = new RestResponse(['data' => $expense->jsonSerialize()]);
    return $response;
  }

  /**
   * Delete an expense
   * 
   * An object containing the expense that was deleted will be returned
   * 
   * @Route("/{id}", name="expense_delete", methods={"DELETE"}, requirements={"id"="\d+"})
   * @OA\Response(
   *    response=200,
   *    description="Expense deleted",
   *    @OA\JsonContent(
   *      @OA\Property(
   *        property="data",
   *        ref=@Model(type=Expense::class)
   *      ),
   *      @OA\Property(
   *        property="response_code",
   *        type="integer",
   *        description="Contains an integer reference to the HTTP response code",
   *        example=200
   *      )
   *    )
   * )
   * @OA\Response(response=500,description="API Exception",@OA\JsonContent(ref="#/components/schemas/APIErrorResponse"))
   * @OA\Response(response=401,description="Authentication Exception",@OA\JsonContent(ref="#/components/schemas/AuthNErrorResponse"))
   * @OA\Response(response=403,description="Authorization Exception",@OA\JsonContent(ref="#/components/schemas/AuthZErrorResponse"))
   * @OA\Parameter(
   *    name="name",
   *    in="query",
   *    @OA\Schema(ref="#/components/schemas/Expense/properties/name")
   * )
   * @OA\Parameter(
   *    name="value",
   *    in="query",
   *    @OA\Schema(ref="#/components/schemas/Expense/properties/value")
   * )
   * @OA\Parameter(
   *    name="description",
   *    in="query",
   *    @OA\Schema(ref="#/components/schemas/Expense/properties/description")
   * )
   */
  public function delete(Expense $expense): Response
  {
    return $this->getResponse('deleteResponse', $expense);
  }

  protected function deleteResponse(Expense $expense): RestResponse
  {
    $entityManager = $this->getDoctrine()->getManager();
    $entityManager->remove($expense);
    $entityManager->flush();

    $response = new RestResponse(['data' => $expense->jsonSerialize()]);
    return $response;
  }
}

<?php

/**
 * @author Callum Smith <callumsmith@me.com>
 * @copyright 2021 Callum Smith-
 */

namespace App\Entity;

use App\Repository\ExpenseRepository;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Annotations as OA;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass=ExpenseRepository::class)
 */
class Expense implements JsonSerializable
{
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   * @OA\Property(description="Expense Identifier")
   */
  private $id;

  /**
   * Name is added as an assumed entity as a description generally is a longtext
   * @ORM\Column(type="string", length=255)
   * @OA\Property(description="Name for the expense",maximum=255)
   */
  private $name;

  /**
   * Value is stored in whole pence
   * @ORM\Column(type="integer")
   * @OA\Property(description="Value of the expense in whole pence")
   */
  private $value;

  /**
   * @ORM\Column(type="text", nullable=true)
   * @OA\Property(description="Long text description of the expense")
   */
  private $description;

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getName(): ?string
  {
    return $this->name;
  }

  public function setName(string $name): self
  {
    $this->name = $name;

    return $this;
  }

  public function getValue(): ?float
  {
    return $this->value;
  }

  public function setValue(float $value): self
  {
    $this->value = $value;

    return $this;
  }

  public function getDescription(): ?string
  {
    return $this->description;
  }

  public function setDescription(?string $description): self
  {
    $this->description = $description;

    return $this;
  }

  public function jsonSerialize()
  {
    return [
      'id' => $this->getId(),
      'name' => $this->getName(),
      'value' => $this->getValue(),
      'description' => $this->getDescription(),
    ];
  }
}
